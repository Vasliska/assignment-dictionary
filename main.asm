
global _start
extern find_word
extern print_string
extern exit
extern read_word

section .rodata
    %include "./words.inc"
	fail: db "error = ", 10, 0
	start: db"write the key -->", 0
	it_is: db "value = ", 0

section .bss
    buffer: resb 255

section .text

_start:
	mov rdi, start
	call print_string

	mov rdi, buffer
	mov rsi, 255
	call read_word

	mov rdi, buffer
	mov rsi,last
	call find_word
	test rdi,rdi
	jnz .ok

	mov rdi, fail
	call print_string
	mov rax, 1
	call exit

	.ok:
		push rdi
		mov rdi, it_is
		call print_string
		pop rdi
		call print_string
		mov rax,0
		call exit

